package org.openqa.selenium.example;

import org.junit.Test;

import com.thoughtworks.selenium.SeleneseTestCase;

@SuppressWarnings("deprecation")
public class SelnTc extends SeleneseTestCase {
	
	public static String googleURL = "http://google.com";
	public static String enterPriseConsultingURL = "http://www.enterpriseconsulting.es/ca";
	public static String sopraURL = "http://www.sopra.es/es";
	public static String[] URLs = {googleURL, enterPriseConsultingURL, sopraURL};
	
	public static String MozillaFirefox = "*firefox";
	public static String GoogleChrome = "*chrome";
	public static String InternetExplorer = "*iexplorer";
	public static String[] Browsers = {GoogleChrome, MozillaFirefox, InternetExplorer};
	
	public static SeleneseTestCase st = new SelnTc();
	public static void main(String args[]){
		st.run();
	}
	
	@Test
	public void TestAllURL_AllBrowsers() throws Exception{
		for(String URL : URLs)
			for(String Browser: Browsers){
				setUp(URL, Browser);
				selenium.start();
				selenium.open("/");
			}
				
		
	}

}
