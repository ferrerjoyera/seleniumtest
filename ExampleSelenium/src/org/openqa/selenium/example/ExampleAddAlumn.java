package org.openqa.selenium.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class ExampleAddAlumn  {
    public static void main(String[] args) {
    	int id=9;
    	String nombre = "oscar";
    	String apellidos = "Ferrer";
    	String dni = "36936969";

    	WebDriver driver = new HtmlUnitDriver();

        driver.get("http://localhost:8080/AddAlumn/AddAlumnPage.jsp");

        WebElement element = driver.findElement(By.name("id"));
        element.sendKeys(String.valueOf(id));
        
        element = driver.findElement(By.name("nombre"));
        element.sendKeys(nombre);
        
        element = driver.findElement(By.name("apellidos"));
        element.sendKeys(apellidos);
        
        element = driver.findElement(By.name("dni"));
        element.sendKeys(dni);
        
        element.submit();

//        System.out.println("Page title is: ".concat(driver.getTitle()));
        
        String resultPageSource = driver.getPageSource();
        if(resultPageSource.contains("Alumno a�adido")){
        	System.out.println("Test passed!");
        } else System.err.println("Test failed!!");
//        System.out.println("Page Source is:\n".concat(resultPageSource));

        driver.quit();
    }
}