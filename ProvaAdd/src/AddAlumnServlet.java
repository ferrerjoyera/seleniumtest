import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddAlumnServlet
 */
@SuppressWarnings("unused")
@WebServlet("/AddAlumnServlet")
public class AddAlumnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
//	private Map<String, String[]> parameterMap;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAlumnServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		attendRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		attendRequest(request, response);
	}
	
	protected void attendRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));
		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		String dni = request.getParameter("dni");
		
		AlumnPrueba ap = new AlumnPrueba(nombre, apellidos, dni, id);
		
//		Map<String, String[]> params = request.getParameterMap();
//		for(String key: params.keySet()){
//			String[] strs = params.get(key);
//			pw.println("Key: "+key);
//			for(String str : strs)
//				pw.println("  "+str);
//		}
			
//		parameterMap = request.getParameterMap(); problema con el parameter map... version de java??
//		parameterMap.
//		request
		pw.println("Alumno a�adido!\n".concat(ap.toString()));
	}

}
