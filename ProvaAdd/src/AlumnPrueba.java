
public class AlumnPrueba {

	private String nombre;
	private String apellidos;
	private String dni;
	private int id;
	
	public AlumnPrueba(String nombre, String apellidos, String dni, int id){
		setId(id);
		setNombre(nombre);
		setApellidos(apellidos);
		setDni(dni);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "AlumnPrueba [nombre=" + nombre + ", apellidos=" + apellidos
				+ ", dni=" + dni + ", id=" + id + ", getNombre()="
				+ getNombre() + ", getApellidos()=" + getApellidos()
				+ ", getDni()=" + getDni() + ", getId()=" + getId()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
}
